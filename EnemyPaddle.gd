extends KinematicBody2D

export (int) var speed

var direction

func set_direction(new_direction):
	direction = new_direction

func _ready():
	pass

func _process(delta):
	direction = null
	if Input.is_action_pressed("ui_up"):
		set_direction("up")
	if Input.is_action_pressed("ui_down"):
		set_direction("down")
	
	if direction == "up":
		position.y -= speed * delta
	elif direction == "down":
		position.y += speed * delta

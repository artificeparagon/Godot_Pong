extends KinematicBody2D

export (int) var speed

var direction

func set_direction(new_direction):
	direction = new_direction

func _ready():
	pass

func _process(delta):
	if direction == "up":
		position.y -= speed * delta
	elif direction == "down":
		position.y += speed * delta

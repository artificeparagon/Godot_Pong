extends Node

var player_score = 0
var enemy_score = 0
var round_started

func _ready():
	new_round()
	pass

func update_score():
	$GUI/PlayerScore.text = str(player_score)
	$GUI/EnemyScore.text = str(enemy_score)

func _process(delta):
	if not round_started:
		if Input.is_action_pressed("ui_accept"):
			round_started = true
			$Ball.moving = true
			$SpaceToStart.visible = false

func new_round():
	update_score()
	var vp = get_viewport().size
	var ball = $Ball
	ball.position = Vector2(vp/2)
	ball.moving = false
	ball.speed.y = 0
	round_started = false
	$SpaceToStart.visible = true
	pass

func _on_ScoreZone_player_scored():
	enemy_score = enemy_score + 1
	$pointScoredAudioPlayer.play()
	new_round()

func _on_ScoreZone_enemy_scored():
	player_score = player_score + 1
	$pointScoredAudioPlayer.play()
	new_round()

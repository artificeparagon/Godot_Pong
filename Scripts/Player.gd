extends Node2D

onready var paddle = $Paddle

func _ready():
	pass

func _process(delta):
	paddle.direction = null
	if Input.is_action_pressed("ui_up"):
		set_direction("up")
	if Input.is_action_pressed("ui_down"):
		set_direction("down")

func set_direction(dir):
	paddle.direction = dir
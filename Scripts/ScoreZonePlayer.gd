extends Area2D

signal player_scored

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_ScoreZone_body_entered(body):
	if body.get_name() == "Ball":
		emit_signal('player_scored')

extends Node2D

onready var paddle = $Paddle
onready var ball = $"../Ball"

var upper
var lower

func _ready():
	pass

func _process(delta):
	paddle.direction = null
	
	upper = paddle.position.y + 10
	lower = paddle.position.y - 10
	
	if ball.position.y < lower:
		paddle.direction = "up"
	elif ball.position.y > upper:
		paddle.direction = "down"
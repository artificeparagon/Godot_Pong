extends KinematicBody2D

export (float) var v_scale
var moving

var speed = Vector2()

func _ready():
	speed.x = -250
	moving = false
	pass

func _process(delta):
	pass

func _physics_process(delta):
	var collision_info
	if(moving):
		collision_info = move_and_collide(speed * delta)
	if collision_info:
		$hitSound.play()
		if collision_info.collider.get_name() == "Paddle":
			speed.x *= -1
			var collider_y = collision_info.collider.position.y
			var difference_y = position.y - collider_y
			speed.y = difference_y * v_scale
		elif collision_info.collider.get_name() == "Wall" || collision_info.collider.get_name() == "Wall2":
			speed.y *= -1